package ru.god.randomizer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Класс, демонстрирующий способ работы с файлами извне на примере рандомайзера для списка фамилий студентов.
 * Фамилии студентов поочередно выводятся в рандомном порядке до тех пор, пока список не окажется пуст.
 *
 * @author Горбачева, 16ИТ18к
 */
public class Randomizer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            /*
            Для того, чтобы можно было работать со строковой информацией из файла list.txt, используем интерфейс List,
            создаем переменную list ссылочного типа со ссылкой на расширяемый массив, чтобы туда можно было добавить
            n-ное количество элементов в виде строк. Файл list.txt содержит 22 строки и выглядит следующим образом:
            Алтабаева
            Андрюшин
            Воеводин
            Волков
            Ворожейкин
            Горбачева
            Горшков
            Гришин
            Гусаров
            Дудаков
            Испольнов
            Каверзин
            Кожевников
            Комиссарова
            Куликов
            Проничкин
            Соломина
            Стрыгин
            Урядов
            Хазипов
            Челноков
            Шишкова
            Импортируя класс Files, мы получаем возможность работать с файлами. Метод readAllLines позволяет считывать
            поочередно каждую строку файла и заносить как отдельный элемент в расширяемый массив, ссылка на который
            хранится в переменной list. Чтобы получить доступ к файлу через указанный путь, нужно импортировать класс
            Paths. Указав в качестве аргумента к методу get путь к файлу D:\work\list.txt, мы можем получить из него
            данные.
             */
            List<String> list = Files.readAllLines(Paths.get("D:\\work\\list.txt"));
            System.out.println("Чтобы получить рандомную фамилию, нажмите 1");
            byte command;
            String print;
            do {
                command = scanner.nextByte();
                print = list.get((int) (Math.random() * list.size()));
                System.out.println(print);
                list.remove(print);
            } while ((!list.isEmpty()));
            System.out.println("Список учеников пуст");
        }
        /*
        В том случае, если в указании пути к файлу допущена ошибка, или файл не существует, или произошла другая ошибка,
        то это обрабатывается как исключительная ситуация и пользователю выдается сообщение об ошибке.
         */ catch (IOException e) {
            System.out.println("Произошла ошибка");
        }
    }
}

/*
public static List<String> readAllLines(Path path, Charset cs) throws IOException {
        try (BufferedReader reader = newBufferedReader(path, cs)) {
            List<String> result = new ArrayList<>();
            for (;;) {
                String line = reader.readLine();
                if (line == null)
                    break;
                result.add(line);
            }
            return result;
        }
    }
*/